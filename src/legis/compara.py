import yaml
import sys
import os

from .misc import condicional


DEBUG = os.environ.get('DEBUG', False)
DEBUG_PERSONA = os.environ.get('DEBUG_PERSONA', False)
SKIP_DOCUMENTS = os.environ.get('SKIP_DOCUMENTS', False)
claves = []


def debug(msg, tipo=''):
    """Muestra ``msg`` si ``DEBUG`` es ``True``.

    Si ``tipo`` es ``PERSONA``, ``msg`` se muestra si
    ``DEBUG_PERSONA`` es ``True``."""
    if DEBUG:
        print(msg)
    elif DEBUG_PERSONA and tipo == 'PERSONA':
        print(msg)

def invierte_fecha(documento, campo):
    """Convierte una fecha de `dd-mm-yyyy` a `yyyy-mm-dd`.

    ``campo`` puede ser `fecha_registro` o `fecha_sesion`.

    """
    date_fields = reversed(documento[campo].split('/'))
    documento[campo] = '-'.join(date_fields)

def actualiza_columna(documento, row, clave, legislatura, db, key, dry_run, yes):
    """Actualiza la columna ``key`` de ``row`` en la base de datos ``db``.

    Pregunta al usuario si acepta actualizar la columna y hace el
    cambio en el renglón con ``id`` igual a ``row['id']`` en la tabla
    ``documentos`` de ``db``.

    Con ``dry_run`` solamente muestra `skipping`.  Con ``yes``
    actualiza sin preguntar.

    """
    PERIOD = legislatura.upper()
    rows = db.get_documento_id(row['id'])
    print(tuple(rows[0]), len(rows))
    if condicional(f'Actualizar {key} en este renglón? [Y/n]', ['', 'y'],
                   dry_run=dry_run, yes=yes):
        print('execute update')
        rowcount = db.update_documento_id(row['id'],
                                          key, documento[key])
        print(rowcount)
        rows = db.get_documento_id(row['id'])
        if rows:
            print(tuple(rows[0]), len(rows))
        else:
            print('0 rows')
    else:
        print('skipping')

def compare(documento, row, clave, legislatura, db, dry_run, yes, verbose):
    """Compara ``documento`` contra ``row`` columna por columna.

    Itera en cada columna de ``documento``. Si el valor de esa columna
    en ``row`` es diferente, muestra la diferencia y llama
    :func:`actualiza_columna`.

    Con ``dry_run`` solamente muestra las diferencias en cada columna
    sin actualizar.  Con ``yes`` muestra la diferencia en cada columna
    y actualiza.

    """
    PERIOD = legislatura.upper()
    igual = True
    diferencias = 0
    for key in documento:
        valor = str(documento[key]) if key=='numero' and documento[key]!=None else documento[key]
        if valor == row[key]:
            debug(f'            {key}: {documento[key]}')
        else:
            if key != 'enlace':
                print(f'       {key:9}: {row[key]!s:30} -> {documento[key]!s:16}')
            else:
                url = '../../agendakioskos/documentos/sistemaintegral/estados/'
                print(f"       {key:9}: {(row[key] or '').replace(url, ''):30} -> {(documento[key] or '').replace(url, ''):16}")
            if not dry_run:
                actualiza_columna(documento, row, clave, legislatura, db, key, dry_run, yes)
            diferencias += 1
    if diferencias/len(documento) > .9:
        igual = False
    if igual:
        if diferencias:
            print(f'----- {diferencias} cambios')
        elif verbose > 1:
            print('----- igual')
    else:
        print('----- diferente')
    return igual


def esInicitiva(record):
    """Regresa ``True`` si el proceso ``record`` es iniciativa de ley.

    Examina el ``tipo`` del primer documento de ``record``, que es el
    primer documento del proceso.

    """
    tipo = record['documentos'][0]['tipo']
    result = 'Iniciativa de Ley' in tipo
    return result


def agrega_documento(legislatura, clave, documento, db, verbose):
    """Inserta ``documento`` en la base de datos ``db``.

    Muestra la cantidad de renglones modificados, que deberá ser 1,
    consulta la tabla de documentos correspondiente a ``legislatura``,
    ``clave``, `estado`, `condicion` y `fecha_registro` y muestra la
    cantidad de documentos, que deberá ser 1.

    """
    rowcount = db.insert_documento(legislatura, clave,
                           documento['enlace'],
                           documento['numero'],
                           documento['fecha_registro'],
                           documento['tipo'],
                           documento['estado'],
                           documento['condicion'],
                           documento['sesion'],
                           documento['fecha_sesion'],
                           documento['votacion'])
    print(rowcount)
    if verbose:
        rows = db.get_documento(legislatura, clave, documento['tipo'],
                        documento['estado'],
                        documento['condicion'],
                        documento['fecha_registro'])
        print(tuple(rows[0]), len(rows))


def procesa_documentos(legislatura, clave, record, documentos_dic, db,
                       dry_run, yes, verbose):
    """Agrega o actualiza documentos del proceso ``clave`` en la base de datos ``db``.

    Itera en los documentos de ``record`` y compara con los documentos
    restantes en ``documentos_dic[clave]``.  Si es igual o una o
    más columnas cambian y se actualizaron, se remueve de los 
    documentos restantes.

    Si ya no hay documentos restantes, es un documento nuevo.  Se
    muestran sus datos y se pregunta al usuario si acepta agregarlo
    al proceso ``clave`` en la base de datos.

    Con ``dry_run`` solamente muestra los datos de cada documento
    nuevo sin agregarlo.  Con ``yes`` muestra cada documento nuevo y
    lo agrega.

    """
    PERIOD = legislatura.upper()
    documentos_db = documentos_dic.get(clave, [])
    restantes_db = []
    restantes_db.extend(documentos_db)
    documentos_web = record['documentos']
    if verbose:
        print(f'procesa_documentos: {len(documentos_db)} db, {len(documentos_web)} web')
    for documento in documentos_web:
        invierte_fecha(documento, 'fecha_registro')
        invierte_fecha(documento, 'fecha_sesion')
        tipo = documento['tipo']
        if not restantes_db:
            print('----- nuevo documento')
            for key in documento:
                if verbose:
                    print(f'            {key}: {documento[key]}')
            if condicional('Agregar este paso? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('execute insert')
                agrega_documento(PERIOD, clave, documento, db, verbose)
        for row in restantes_db:
            if compare(documento, row, clave, legislatura, db, dry_run, yes, verbose):
                restantes_db.remove(row)
                break

def procesa_autores(legislatura, clave, record, db, dry_run, yes, verbose):
    """Agrega autores a la base de datos ``db``.

    Itera en cada autor nuevo en ``record``.  Si no está en la tabla
    ``persona``, pregunta al usuario si acepta agregarlo.  Si no está
    asociado al proceso ``clave`` de ``legislatura``, pregunta al
    usuario si acepta asociarlo.

    Con ``dry_run`` solamente muestra el autor sin agregarlo.  Con
    ``yes`` muestra cada autor nuevo y lo agrega y asocia.

    """
    if verbose:
        print('procesa_autores')
    personas = [row['nombre'] for row in db.get_personas()]
    autores = [row['nombre'] for row in db.get_autores(legislatura, clave)]
    for autor in autores:
        if autor not in record['autores']:
            print(f'  remover autor: {autor}')
            if condicional(' Remover autor? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('  removiendo')
                db.remueve_autor(legislatura, clave, autor)
    for autor in record['autores']:
        if autor in personas:
            debug(f'  existe: {autor}', tipo='PERSONA')
        else:
            print(f'  nueva persona: {autor}')
            if condicional(' Agregar persona? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('  agregando')
                db.inserta_persona(autor)
        if autor not in autores:
            print(f'  nuevo autor: {autor}')
            if condicional(' Agregar autor? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('  agregando')
                db.inserta_autor(legislatura, clave, autor)

def procesa_adhesiones(legislatura, clave, record, db, dry_run, yes, verbose):
    """Agrega adhesiones a la base de datos ``db``.

    Itera en cada adhesión nueva en ``record``.  Si no está en la tabla
    ``persona``, pregunta al usuario si acepta agregarla.  Si no está
    asociada al proceso ``clave`` de ``legislatura``, pregunta al
    usuario si acepta asociarla.

    Con ``dry_run`` solamente muestra la adhesión sin agregarla.  Con
    ``yes`` muestra cada adhesión nueva y la agrega y asocia.

    """
    if verbose:
        print('procesa_adhesiones')
    personas = [row['nombre'] for row in db.get_personas()]
    adhesiones = [row['nombre'] for row in db.get_adhesiones(legislatura, clave)]
    if verbose > 1:
        print(adhesiones)
    for adhesion in record['adhesiones']:
        if adhesion in personas:
            debug(f'  existe: {adhesion}', tipo='PERSONA')
        else:
            print(f'  nueva persona: {adhesion}')
            if condicional(' Agregar persona? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('  agregando')
                db.inserta_persona(adhesion)
        if adhesion in adhesiones:
            debug(f'  existe: {adhesion}')
        else:
            print(f'  nueva adhesión: {adhesion}')
            if condicional(' Agregar adhesión? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('  agregando')
                db.asigna_adhesion(legislatura, clave, adhesion)


def procesa_comisiones(legislatura, clave, record, db, dry_run, yes, verbose):
    """Agrega comisiones a la base de datos ``db``.

    Itera en cada comisión nueva en ``record``.  Si no está en la tabla
    ``comisiones``, pregunta al usuario si acepta agregarla.  Si no está
    asociada al proceso ``clave`` de ``legislatura``, pregunta al
    usuario si acepta asociarla.

    Con ``dry_run`` solamente muestra la comisión sin agregarla.  Con
    ``yes`` muestra cada comisión nueva y la agrega y asocia.

    """
    if verbose:
        print('procesa_comisiones')
    comisiones_todas = [row['nombre'] for row in db.get_comisiones(legislatura)]
    comisiones = [row['nombre'] for row in db.get_comisiones_asignadas(legislatura, clave)]
    if verbose > 1:
        print(comisiones)
    for comision in record['comisiones']:
        if comision in comisiones_todas:
            debug(f'  existe: {comision}')
        else:
            print(f'  nueva comisión: {comision}')
            if condicional(' Agregar comisión? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('  agregando')
                db.inserta_comisiones(comision, legislatura)
                comisiones_todas = [row['nombre'] for row in db.get_comisiones(legislatura)]
        if comision in comisiones:
            debug(f'  asignada: {comision}')
        else:
            print(f'  asignar comisión: {comision}')
            if condicional(' Asignar comisión? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('  asignando')
                db.asigna_comision(legislatura, clave, comision)

def procesa_proceso(legislatura, clave, ini_dict, record, db, dry_run, yes, verbose):
    """Agrega el proceso ``clave`` a la base de datos ``db`` si es nuevo.

    Muestra ``clave`` y los datos del proceso, y pregunta al usuario
    si acepta agregarlo a la base de datos.

    Con ``dry_run`` solamente muestra los datos de cada proceso nuevo
    sin agregarlo.  Con ``yes`` muestra cada proceso nuevo y lo
    agrega.

    """

    if clave not in ini_dict:
        print(f'nuevo proceso: {clave}')
        for key in record:
            if verbose:
                print(f'            {key}: {record[key]}')
        if not dry_run:
            if condicional('Agregar este proceso? [Y/n]', ['', 'y'],
                           dry_run=dry_run, yes=yes):
                print('inserta')
                invierte_fecha(record, 'fecha_ingreso')
                db.inserta_proceso(legislatura, clave,
                                   record['descripcion'],
                                   record['fecha_ingreso'])
    else:
        descripcion_actual = ini_dict[clave]['descripcion']
        descripcion = record['descripcion']
        if descripcion != descripcion_actual:
            print('descripcion:')
            print(descripcion_actual, '->')
            print(descripcion)
            if not dry_run:
                if condicional(f'Actualizar descripción? [Y/n]', ['', 'y'],
                               dry_run=dry_run, yes=yes):
                    print('db.update')
                    rows = db.update_proceso(legislatura, clave, 'descripcion',
                                             descripcion)
                    print(rows, 'rows')
                else:
                    print('skipping')


def process_yaml_file(yaml_file_name, legislatura, ini_dict, documentos_dic, db, dry_run, yes, verbose, entidades, registros):
    """Itera los procesos de ``yaml_file_name`` y procesa ``entidades``.

    Toma solamente los procesos que son iniciativa de ley.  Procesa
    cada elemento de ``entidades``: procesos, pasos, autores,
    adhesiones y comisiones, llamando :func:`procesa_proceso`,
    :func:`procesa_documentos`, :func:`procesa_autores`,
    :func:`procesa_adhesiones` y :func:`procesa_comisiones`
    respectivamente..

    """

    legislatura = legislatura.upper()
    yaml_file = open(yaml_file_name)
    records = yaml.load(yaml_file, Loader=yaml.Loader)

    for record in records:
        clave = record['clave']
        if clave in claves:
            continue
        else:
            claves.append(clave)
        if esInicitiva(record) or record['clave'] in registros:
            print(f'{clave}')
            if 'procesos' in entidades:
                procesa_proceso(legislatura, clave, ini_dict, record, db,
                                dry_run, yes, verbose)
            if 'pasos' in entidades:
                procesa_documentos(legislatura, clave, record, documentos_dic,
                                   db,dry_run, yes, verbose)
            if 'autores' in entidades:
                procesa_autores(legislatura, clave, record, db,
                                dry_run, yes, verbose)
            if 'adhesiones' in entidades:
                procesa_adhesiones(legislatura, clave, record, db,
                                   dry_run, yes, verbose)
            if 'comisiones' in entidades:
                procesa_comisiones(legislatura, clave, record, db,
                                   dry_run, yes, verbose)


def compara_yaml_bd(legislatura, db, archivos_yaml, dry_run, yes,
                    verbose, entidades, registros):
    """Itera en los archivos YAML para comparar con la base de datos.

    Llama :func:`process_yaml_file` para cada archivo YAML."""

    period = legislatura.periodo
    PERIOD = period.upper()
    print('carga iniciativas de', PERIOD)
    print(len(legislatura.ini_dict), 'iniciativas')
    ini_dict = legislatura.ini_dict
    if registros:
        documentos = legislatura.doc_dict
    else:
        documentos = legislatura.doc_dict
    documentos_dic = legislatura.documentos_dic

    for yaml_file_name in archivos_yaml:
        print('--', yaml_file_name)
        process_yaml_file(yaml_file_name, period, ini_dict,
                          documentos_dic, db, dry_run, yes, verbose, entidades,
                          registros)
    print('Fin')
