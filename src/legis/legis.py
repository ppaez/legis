import re


def descripcion_corta(texto):
    'Genera y regresa descripción corta'

    texto = re.sub(r' ?\(f\.? ?[0-9]+\)', '', texto, flags=re.I)
    texto = re.sub('.  ?presentada mediante oficio .*', '.', texto, flags=re.I)
    texto = re.sub(r'\((cd?|usb)/anexo\)', '', texto, flags=re.I)
    texto = re.sub('propone reformar', 'reforma', texto)
    texto = re.sub('propone adicionar', 'adiciona', texto)
    texto = re.sub('mediante el cual', 'que', texto)
    texto = re.sub('solicita corregir', 'corrige', texto)
    m = re.search('para ((reformar[,;]?|adicionar|derogar) .*)', texto)
    if m:
        resultado = m.group(1)
        return resultado[0].upper() + resultado[1:]
    que_regex = 'que  *((reforman?|modifica|deroga|adiciona|expide|crea|' \
        'abroga|aprueba|reforma,|corrige) .*)'
    m = re.search(que_regex, texto, flags=re.I)
    if m:
        resultado = m.group(1)
        return resultado[0].upper() + resultado[1:]
    se_regex = 'se modifican( .*)'
    m = re.search(se_regex, texto)
    if m:
        resultado = m.group(1)
        return 'Modifica' + resultado
    se_regex = 'se (reforma .*)'
    m = re.search(se_regex, texto)
    if m:
        resultado = m.group(1)
        return resultado[0].upper() + resultado[1:]
    se_regex = 'modificación a (.*)'
    m = re.search(se_regex, texto)
    if m:
        return 'Modifica ' + m.group(1)
    else:
        resultado = texto
    return resultado
