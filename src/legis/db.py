import sqlite3
from datetime import date


def comando_insert(tabla, columnas):
    'Genera comando para insertar'

    lista_columnas = ','.join(columnas)
    lista_valores = ','.join('?'*len(columnas))
    cmd = f'INSERT INTO {tabla}({lista_columnas}) VALUES ({lista_valores})'
    return cmd


class DB:
    '''Realiza operaciones en una base de datos sqlite'''

    def __init__(self, ruta_base=None):
        'Conecta a la base de datos y crea un cursor'
        self.status = ''
        self.connection = sqlite3.connect(database=ruta_base)
        self.connection.row_factory = sqlite3.Row
        self.cursor = self.connection.cursor()
        self.cursor.execute('PRAGMA foreign_keys=ON;')

    def inserta_proceso(self, legislatura, clave, descripcion, fecha_ingreso):
        ''
        columnas = ('legislatura', 'clave', 'descripcion', 'fecha_ingreso')
        valores = (legislatura, clave, descripcion, fecha_ingreso)
        self.insertar('registros', columnas, valores)

    def update_proceso(self, legislatuva, clave, nombre_columna, valor):
        cmd = f"UPDATE registros SET {nombre_columna}=? WHERE legislatura=? AND clave=?"
        self.cursor.execute(cmd, (valor, legislatuva, clave))
        self.connection.commit()
        return self.cursor.rowcount

    def get_procesos(self, legislatura):
        cmd = f"SELECT registros.clave AS clave, fecha_ingreso, descripcion, nota, tags FROM registros WHERE legislatura=?;"
        self.cursor.execute(cmd, (legislatura,))
        records = self.cursor.fetchall()
        return records

    def get_iniciativas(self, legislatura):
        tmpl = f"SELECT DISTINCT registros.clave AS clave, fecha_ingreso, descripcion, nota, tags, resumen, cambios, folio FROM registros CROSS JOIN documentos ON registros.clave=documentos.clave AND registros.legislatura=documentos.legislatura WHERE tipo LIKE 'Iniciativa%ley%' AND registros.legislatura='{legislatura}' ORDER BY CAST (registros.clave AS INT);"
        # cmd = tmpl.format(archivo)
        cmd = tmpl
        self.cursor.execute(cmd)
        records = self.cursor.fetchall()
        return records

    def get_aprobadas(self, legislatura):
        tmpl = f"SELECT DISTINCT registros.clave, fecha_ingreso, descripcion, nota, tags, resumen, cambios, folio FROM documentos JOIN registros ON documentos.clave=registros.clave AND registros.legislatura=documentos.legislatura WHERE registros.clave in (SELECT DISTINCT registros.clave FROM documentos JOIN registros ON documentos.clave=registros.clave AND registros.legislatura=documentos.legislatura WHERE tipo LIKE 'Iniciativa%ley%' AND registros.legislatura='{legislatura}') AND condicion = 'Aprobado' AND tipo = 'Minuta de Decreto' AND documentos.legislatura='{legislatura}' ORDER BY CAST (registros.clave AS INT);"
        # cmd = tmpl.format(archivo)
        cmd = tmpl
        self.cursor.execute(cmd)
        records = self.cursor.fetchall()
        return records

    def get_desechadas(self, legislatura):
        tmpl = f"SELECT DISTINCT registros.clave, fecha_ingreso, descripcion, nota, tags, resumen, cambios, folio FROM documentos JOIN registros ON documentos.clave=registros.clave AND registros.legislatura=documentos.legislatura WHERE registros.clave in (SELECT DISTINCT registros.clave FROM documentos JOIN registros ON documentos.clave=registros.clave AND registros.legislatura=documentos.legislatura WHERE tipo LIKE 'Iniciativa%ley%' AND registros.legislatura='{legislatura}') AND condicion = 'Aprobado' AND tipo = 'Acuerdo Legislativo' AND documentos.legislatura='{legislatura}' ORDER BY CAST (registros.clave AS INT);"
        # cmd = tmpl.format(archivo)
        cmd = tmpl
        self.cursor.execute(cmd)
        records = self.cursor.fetchall()
        return records

    def get_documentos(self, legislatura):
        tmpl = f"SELECT DISTINCT documentos.id, registros.clave, fecha_registro, tipo, condicion, estado, sesion, fecha_sesion, votacion, enlace, numero FROM documentos CROSS JOIN registros ON documentos.clave=registros.clave WHERE registros.clave in (SELECT DISTINCT registros.clave AS clave FROM documentos JOIN registros ON documentos.clave=registros.clave WHERE tipo LIKE 'Iniciativa%ley%' AND registros.legislatura='{legislatura}') AND documentos.legislatura='{legislatura}' ORDER BY CAST (registros.clave AS INT);"
        # cmd = tmpl.format(archivo)
        cmd = tmpl
        self.cursor.execute(cmd)
        records = self.cursor.fetchall()
        return records

    def get_documentos_todos(self, legislatura):
        cmd = f"SELECT id, clave, fecha_registro, tipo, condicion, estado, sesion, fecha_sesion, votacion, enlace, numero FROM documentos WHERE legislatura=?;"
        self.cursor.execute(cmd, (legislatura,))
        records = self.cursor.fetchall()
        return records

    def get_documento(self, legislatura, clave, tipo, estado, condicion, fecha_registro):
        cmd = f"SELECT id, legislatura, clave, fecha_registro, tipo, condicion, estado, sesion, fecha_sesion, votacion, enlace, numero FROM documentos WHERE legislatura=? AND clave=? AND tipo=? AND estado=? AND condicion=? AND fecha_registro=?"
        self.cursor.execute(cmd, (legislatura, clave, tipo, estado, condicion, fecha_registro))
        records = self.cursor.fetchall()
        return records

    def get_documento_id(self, id):
        cmd = f"SELECT id, legislatura, clave, fecha_registro, tipo, condicion, estado, sesion, fecha_sesion, votacion, enlace, numero FROM documentos WHERE id=?"
        self.cursor.execute(cmd, (id,))
        records = self.cursor.fetchall()
        return records

    def update_documento_id(self, id, nombre_columna, valor):
        cmd = f"UPDATE documentos SET {nombre_columna}=? WHERE id=?"
        self.cursor.execute(cmd, (valor, id))
        self.connection.commit()
        return self.cursor.rowcount

    def update_documento(self, legislatura, clave, tipo, estado, condicion,
                         fecha_registro, nombre_columna, valor):
        cmd = f"UPDATE documentos SET {nombre_columna}=? WHERE legislatura=? AND clave=? AND tipo=? AND estado=? AND condicion=? AND fecha_registro=?"
        self.cursor.execute(cmd, (valor, legislatura, clave, tipo, estado, condicion, fecha_registro))
        self.connection.commit()
        return self.cursor.rowcount

    def get_comisiones(self, legislatura):
        cmd = f"SELECT nombre FROM comisiones WHERE legislatura=?"
        self.cursor.execute(cmd, (legislatura,))
        records = self.cursor.fetchall()
        return records

    def get_comisiones_asignadas(self, legislatura, clave=''):
        if clave:
            cmd = f"SELECT nombre FROM registros JOIN comision ON registros.id=comision.registro_id JOIN comisiones ON comisiones.id=comision.comision_id WHERE registros.legislatura=? AND clave=?"
            self.cursor.execute(cmd, (legislatura, clave))
        else:
            cmd = f"SELECT clave, nombre FROM registros LEFT JOIN comision ON registros.id=comision.registro_id LEFT JOIN comisiones ON comisiones.id=comision.comision_id WHERE registros.legislatura=?"
            self.cursor.execute(cmd,  (legislatura,))
        records = self.cursor.fetchall()
        return records

    def get_comisiones_iniciativas(self, legislatura):
        tmpl = f"SELECT DISTINCT nombre, registros.clave FROM registros CROSS JOIN documentos ON documentos.clave=registros.clave CROSS JOIN comision ON registros.id=comision.registro_id CROSS JOIN comisiones ON comisiones.id=comision.comision_id WHERE tipo LIKE 'Iniciativa%ley%' AND registros.legislatura='{legislatura}' ORDER BY nombre"
        cmd = tmpl
        self.cursor.execute(cmd)
        records = self.cursor.fetchall()
        return records

    def insert_documento(self, legislatura, clave, enlace, numero,
                         fecha_registro, tipo, estado, condicion, sesion,
                         fecha_sesion, votacion):
        cmd = f"INSERT INTO documentos (legislatura, clave, enlace, numero, fecha_registro, tipo, estado, condicion, sesion, fecha_sesion, votacion) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        self.cursor.execute(cmd, (legislatura, clave, enlace, numero, fecha_registro, tipo, estado, condicion, sesion, fecha_sesion, votacion))
        self.connection.commit()
        return self.cursor.rowcount

    def get_personas(self):
        cmd = "SELECT nombre FROM persona"
        self.cursor.execute(cmd)
        records = self.cursor.fetchall()
        return records

    def inserta_persona(self, nombre):
        ''
        cmd = f'INSERT INTO persona(nombre) VALUES (?)'
        self.cursor.execute(cmd, (nombre,))
        self.connection.commit()
        return self.cursor.rowcount

    def inserta_comisiones(self, nombre, legislatura):
        ''
        cmd = f'INSERT INTO comisiones(nombre,legislatura) VALUES (?,?)'
        self.cursor.execute(cmd, (nombre, legislatura))
        self.connection.commit()
        return self.cursor.rowcount

    def get_autores(self, legislatura, clave=''):
        if clave:
            cmd = "SELECT persona.nombre, nombre_corto, siglas, color FROM persona JOIN autor ON persona.id=autor.persona_id JOIN registros ON registro_id=registros.id LEFT JOIN afiliacion ON persona.id=afiliacion.persona_id LEFT JOIN partido ON afiliacion.partido_id=partido.id WHERE legislatura=? AND clave=?"
            self.cursor.execute(cmd, (legislatura, clave))
        else:
            cmd = "SELECT persona.nombre, nombre_corto, siglas, color, clave FROM persona JOIN autor ON persona.id=autor.persona_id JOIN registros ON registro_id=registros.id LEFT JOIN afiliacion ON persona.id=afiliacion.persona_id LEFT JOIN partido ON afiliacion.partido_id=partido.id WHERE legislatura=?"
            self.cursor.execute(cmd, (legislatura,))
        records = self.cursor.fetchall()
        return records

    def get_adhesiones(self, legislatura, clave=''):
        if clave:
            cmd = "SELECT persona.nombre, nombre_corto, siglas, color FROM persona JOIN adhesion ON persona.id=adhesion.persona_id JOIN registros ON registro_id=registros.id LEFT JOIN afiliacion ON persona.id=afiliacion.persona_id LEFT JOIN partido ON afiliacion.partido_id=partido.id WHERE legislatura=? AND clave=?"
            self.cursor.execute(cmd, (legislatura, clave))
        else:
            cmd = "SELECT persona.nombre, nombre_corto, siglas, color, clave FROM persona JOIN adhesion ON persona.id=adhesion.persona_id JOIN registros ON registro_id=registros.id JOIN afiliacion ON persona.id=afiliacion.persona_id JOIN partido ON afiliacion.partido_id=partido.id WHERE legislatura=?"
            self.cursor.execute(cmd, (legislatura,))
        records = self.cursor.fetchall()
        return records

    def inserta_autor(self, legislatura, clave, nombre):
        ''
        cmd = f'INSERT INTO autor(registro_id, persona_id) VALUES ((SELECT id FROM registros WHERE legislatura=? AND clave=?), (SELECT id FROM persona WHERE nombre=?))'
        self.cursor.execute(cmd, (legislatura, clave, nombre))
        self.connection.commit()
        return self.cursor.rowcount

    def remueve_autor(self, legislatura, clave, nombre):
        ''
        cmd = f'DELETE FROM autor WHERE registro_id=(SELECT id FROM registros WHERE legislatura=? AND clave=?) AND persona_id=(SELECT id FROM persona WHERE nombre=?)'
        self.cursor.execute(cmd, (legislatura, clave, nombre))
        self.connection.commit()
        return self.cursor.rowcount

    def asigna_comision(self, legislatura, clave, nombre):
        ''
        cmd = f'INSERT INTO comision(registro_id, comision_id) VALUES ((SELECT id from registros WHERE legislatura=? AND clave=?), (SELECT id FROM comisiones WHERE nombre=?))'
        self.cursor.execute(cmd, (legislatura, clave, nombre))
        self.connection.commit()
        return self.cursor.rowcount

    def desasigna_comision(self, legislatura, clave, nombre):
        ''
        cmd = f'DELETE FROM comision WHERE registro_id=(SELECT id from registros WHERE legislatura=? AND clave=?) AND comision_id=(SELECT id FROM comisiones WHERE nombre=?);'
        self.cursor.execute(cmd, (legislatura, clave, nombre))
        self.connection.commit()
        return self.cursor.rowcount

    def asigna_adhesion(self, legislatura, clave, nombre):
        ''
        cmd = f'INSERT INTO adhesion(registro_id, persona_id) VALUES ((SELECT id from registros WHERE legislatura=? AND clave=?), (SELECT id FROM persona WHERE nombre=?))'
        self.cursor.execute(cmd, (legislatura, clave, nombre))
        self.connection.commit()
        return self.cursor.rowcount

    def agrega_afiliacion(self, persona, siglas):
        ''

        cmd = f'INSERT INTO afiliacion(persona_id, partido_id) VALUES ((SELECT id from persona WHERE nombre=?), (SELECT id FROM partido WHERE siglas=?))'
        self.cursor.execute(cmd, (persona, siglas))
        self.connection.commit()
        return self.cursor.rowcount

    def insertar(self, tabla, columnas, valores):
        'Inserta una o más columnas en tabla'

        cmd = comando_insert(tabla, columnas)
        print(cmd)
        self.cursor.execute(cmd, valores)
        self.connection.commit()
        return self.cursor.rowcount


def iniciativa_dict(iniciativas):
    'Convert rows to a mapping of clave to dictionary'
    ini_dict = {}
    for iniciativa in iniciativas:
        ini_dict[iniciativa['clave']] = iniciativa
    return ini_dict


def documento_dict(documentos):
    'Convert rows to a mapping of clave to a list of documents'
    doc_dict = {}
    for documento in documentos:
        if documento['clave'] not in doc_dict:
            doc_dict[documento['clave']] = [dict(documento)]
        else:
            doc_dict[documento['clave']].append(dict(documento))
    return doc_dict


def intervalos_acumulado(documento_dict):
    'Agrega intervalos y el acumulado de días'

    for clave in documento_dict:
        documento_anterior = False
        aprobado = False
        acumulado = 0
        for documento in documento_dict[clave]:
            if documento_anterior and not aprobado:
                fecha_anterior = date.fromisoformat(documento_anterior['fecha_sesion'])
                fecha_actual = date.fromisoformat(documento['fecha_sesion'])
                intervalo = fecha_actual - fecha_anterior
                acumulado += intervalo.days
                documento_anterior['intervalo'] = intervalo.days
            documento_anterior = documento
