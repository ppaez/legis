from time import sleep
import re
import os
from datetime import datetime
from urllib.request import urlopen
from urllib.request import Request
from urllib.error import URLError


UserAgent = os.environ['LEGIS_USERAGENT']
base_url = 'https://congresoweb.congresojal.gob.mx/infolej/sistemaintegral/infolej/'


def descarga_documento(url, pdf_file):
    ''
    request = Request(url, headers={'User-Agent': UserAgent})
    start_urlopen = datetime.now()
    try:
        response = urlopen(request)
        data = response.read()
        end_urlopen = datetime.now()
        interval = end_urlopen - start_urlopen
    except URLError:
        return 'error', 0, 'error', 0, 0
    with open(pdf_file, 'wb') as out:
        out.write(data)
    return 'ok', interval, response.status, response.reason, len(data), response.headers


def descarga_procesos(legislatura, html_dir, dry_run=False):
    ''

    period = legislatura
    print('periodo', period)
    pagina = 1
    pagina_final = 1
    while pagina <= pagina_final:
        html_file_path = f'{html_dir}/{period}/{pagina:03}.html'
        print(f'pagina {pagina} {html_file_path}')
        if not dry_run:
            if os.path.exists(html_file_path):
                print('existe')
                html = open(html_file_path).read()
            else:
                data = f'C_Legis={period.upper()}&C_L=1&C_NumSesion=&C_NumInfolej=&C_Ponente=0&C_Comi=0&C_Tipo=0&C_Estado=0&C_CEstado=0&C_Tema=0&C_palabra=&C_Numero=&C_Fecha=&C_Fecha1=&Busca=1&C_si=2&numregxpag=100&pagina={pagina}'
                print('open url')
                request = Request(base_url+'iniciopw.cfm', headers={'User-Agent': UserAgent})
                start_urlopen = datetime.now()
                response = urlopen(request, data=data.encode())
                end_urlopen = datetime.now()
                interval = end_urlopen - start_urlopen
                print('done', interval)
                print(response.status, response.reason)
                html = response.read().decode()
                out = open(html_file_path, 'w')
                print('save')
                out.write(html)
                print('wait')
                sleep(30)

            if pagina_final == 1:
                pagina_final = int(re.findall('cambiapagina\(([^)]*)\)" class="link_pag">fin', html)[0])
                print('pagina_final', pagina_final)
        pagina += 1

