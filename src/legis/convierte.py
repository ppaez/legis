import sys
import re
from pathlib import Path

import bs4
import yaml


base_url = 'https://congresoweb.congresojal.gob.mx/infolej/sistemaintegral/infolej/'
offset = 4
documentos_headers  = ['tipo', 'estado', 'condicion', 'fecha_registro',
                       'numero', 'sesion', 'fecha_sesion', 'votacion']

def get_text(columna):
    ''
    enlace = columna.find('a')
    texto = columna.string
    script = columna.find('script')
    if script:
        script = script.string.strip()
        script = re.findall("'([^']*)'", script)[1]
    if enlace:
        url = enlace.get('href')
        onclick = enlace.get('onclick').replace("window.open('", '').replace("','_blank')", '')
        if url:
            texto = url
        elif onclick:
            texto = onclick
    elif script:
        texto = script
    if not texto:
        texto = ''
    return texto.strip()


def read_block(tables, block):
    'Regresa un mapeo de datos de un proceso'
    record = {}
    i = block * 4 + offset
    clave = tables[i]('td')[0].string.replace('No. INFOLEJ: ', '')
    record['clave'] = re.sub('([^/]+)/.*', r'\1', clave)
    text  = ''
    for span in tables[i+1]('span')[:2]:
        if span.string:
            text += span.string
    record['descripcion'] = text.strip()
    record['fecha_ingreso'] = str(tables[i+1]('tr')[1]('td')[0].string)
    estados = tables[i+2]('tr')[1::2]
    autores_comisiones = tables[i+3]
    autores = list(autores_comisiones('tr')[1]('td')[0].strings)
    record['autores'] = [e.strip().replace(' / Autor', '') for e in autores if ' / Autor' in e]
    record['adhesiones'] = [e.strip().replace(' / Adhesion', '') for e in autores if ' / Adhesion' in e]
    comisiones = autores_comisiones('tr')[1]('td')[1].strings
    record['comisiones'] = [e.strip() for e in comisiones if e.strip()]
    diarios = autores_comisiones('tr')[3]('td')[1]('a')
    record['documentos'] = []
    for estado in estados:
        document = {}
        columnas = estado('td')
        a = columnas[0].find('a')
        if a:
            enlace = a.get('href')
        else:
            enlace = 'none'
        document['enlace'] = enlace
        for header, columna in zip(documentos_headers, columnas[1:]):
            texto = get_text(columna)
            document[header] = texto
        record['documentos'].append(document)
    record['documentos'].reverse()
    record['diarios'] = []
    for diario in diarios:
        enlace = diario['onclick'].replace("window.open('", '').replace("','_blank')", '')
        record['diarios'].append(enlace)
    return record


def lee_html(ruta_archivo_html):
    ''
    procesos = []
    text = open(ruta_archivo_html).read()
    soup = bs4.BeautifulSoup(text, features='html.parser')
    tables = soup('table')
    blocks = (len(tables) - offset)//4
    for block in range(blocks):
        record  = read_block(tables, block)
        procesos.append(record)
    return procesos


def salida_yaml(procesos, stream):
    ''
    for record in procesos:
        print('-', file=stream)
        print(f"  clave: '{record['clave']}'", file=stream)
        descripcion = record['descripcion']
        if ':' in descripcion:
            descripcion = "'" + descripcion + "'"
        descripcion = descripcion.replace('\r', '')
        descripcion = descripcion.replace('\n', '')
        descripcion = descripcion.replace('\t', ' ')
        descripcion = descripcion.replace('|', '')
        print('  descripcion:', descripcion, file=stream)
        print('  fecha_ingreso:', record['fecha_ingreso'], file=stream)
        print('  documentos:', file=stream)
        for documento in record['documentos']:
            print('    -', file=stream)
            for columna in documento:
                valor = documento[columna]
                if valor == '':
                    valor = "''"
                print('     ', columna +':' , valor, file=stream)
        if record['autores']:
            print('  autores:', file=stream)
            for autor in record['autores']:
                print('    -', autor, file=stream)
        else:
            print('  autores: []', file=stream)
        if record['adhesiones']:
            print('  adhesiones:', file=stream)
            for autor in record['adhesiones']:
                print('    -', autor, file=stream)
        else:
            print('  adhesiones: []', file=stream)
        if record['comisiones']:
            print('  comisiones:', file=stream)
            for comision in record['comisiones']:
                print('    -', comision, file=stream)
        else:
            print('  comisiones: []', file=stream)
        if record['diarios']:
            print('  diarios:', file=stream)
            for diario in record['diarios']:
                if diario == '':
                    diario = "''"
                print('    -', diario, file=stream)
        else:
            print('  diarios: []', file=stream)


def convierte_html_yaml(ruta_html, ruta_yaml, legislatura):
    ''
    print('convierte', ruta_html, ruta_yaml)
    archivos_html = sorted((Path(ruta_html) / Path(legislatura)).glob('*.html'))
    for archivo_html in archivos_html:
        print(archivo_html, end='')
        procesos = lee_html(archivo_html)
        nombre_yaml = Path(archivo_html.stem).with_suffix('.yaml')
        archivo_yaml = (Path(ruta_yaml) / Path(legislatura) / nombre_yaml)
        print('->', archivo_yaml, end='')
        stream = open(archivo_yaml, 'w')
        salida_yaml(procesos, stream)
        print(' done')
