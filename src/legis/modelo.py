from datetime import date
import re
from collections import defaultdict


def estado_iniciativa(doc_dict, clave):
    '''Determina el estado de la iniciativa clave

    Regresa `Pendiente`, `Desechado` o `Aprobado`'''

    for documento in doc_dict[clave]:
        if documento['condicion'] == 'Aprobado':
            if documento['tipo'] == 'Minuta de Decreto' and \
               documento['estado'] in ['Concluido', 'Estudio']:
                return 'Aprobado'
            elif documento['tipo'] == 'Acuerdo Legislativo' and \
               documento['estado'] == 'Concluido':
                return 'Desechado'
    return 'Pendiente'


def totales(l):
    '''Obtiene y regresa totales de iniciativas.

    Calcula iniciativas presentadas, aprobadas y desechadas.
    Separa las iniciativas de leyes de ingresos municipales.'''
    ingresos = defaultdict(int)
    otras = defaultdict(int)
    ingresos['Total'] = ingresos['Aprobado'] = 0
    ingresos['Desechado'] = ingresos['Pendiente'] = 0
    otras['Total'] = otras['Aprobado'] = 0
    otras['Desechado'] = otras['Pendiente'] = 0
    for numero in l.ini_dict:
        estado = l.estado_dict[numero]
        if l.ini_dict[numero]['municipal']:
            ingresos[estado] += 1
            ingresos['Total'] += 1
        else:
            otras[estado] += 1
            otras['Total'] += 1
    for estado in ('Aprobado', 'Desechado', 'Pendiente'):
        if ingresos['Total']:
            ingresos[estado+'_porc'] = round(
                ingresos[estado] / ingresos['Total'] * 100, 1)
        else:
            ingresos[estado+'_proc'] = 0
        if otras['Total']:
            otras[estado+'_porc'] = round(
            otras[estado] / otras['Total'] * 100, 1)
        else:
            otras[estado+'_porc'] = 0
    return ingresos, otras


class Legislatura:
    '''Información acerca de una legislatura

    Atributos:

    ``comisiones_iniciativas_dict``: mapa de nombre a lista de números
    de procesos asignados.

    '''

    def __init__(self, periodo):
        self.periodo = periodo
        self.procesos = {}
        self.ini_dict = {}

    def __repr__(self):
        return f'Legislatura {self.periodo}, {len(self.ini_dict)} iniciativas'

    def carga(self, db):
        'Carga de la base de datos'
        PERIODO = self.periodo.upper()

        iniciativas = db.get_iniciativas(PERIODO)
        ini_dict = list2dict(iniciativas)
        self.ini_dict = ini_dict

        documentos = db.get_documentos(PERIODO)
        acumulado_dict, doc_dict = calcula_intervalos_acumulado(documentos)
        self.acumulado_dict = acumulado_dict
        self.doc_dict = doc_dict
        self.estado_dict = {}
        for numero in self.ini_dict:
            self.estado_dict[numero] = estado_iniciativa(doc_dict, numero)

        self.aprobadas_dict = {}
        self.desechadas_dict = {}
        self.pendientes_dict = {}
        for numero in self.ini_dict:
            if self.estado_dict[numero] == 'Aprobado':
                self.aprobadas_dict[numero] = self.ini_dict[numero]
            elif self.estado_dict[numero] == 'Desechado':
                self.desechadas_dict[numero] = self.ini_dict[numero]
            else:
                self.pendientes_dict[numero] = self.ini_dict[numero]

        cant_iniciativas = len(self.ini_dict)
        cant_aprobadas = len(self.aprobadas_dict)
        cant_desechadas = len(self.desechadas_dict)
        cant_inconclusas = cant_iniciativas - cant_aprobadas - cant_desechadas
        porcentaje_aprobadas = 0
        if cant_iniciativas:
            porcentaje_aprobadas = round(cant_aprobadas/cant_iniciativas*100, 1)
        porcentaje_desechadas = 0
        if cant_iniciativas:
            porcentaje_desechadas = round(cant_desechadas/cant_iniciativas*100, 1)
        porcentaje_inconclusas = 0
        if cant_iniciativas:
            porcentaje_inconclusas = round(cant_inconclusas/cant_iniciativas*100, 1)
        self.cant_iniciativas = cant_iniciativas
        self.cant_aprobadas = cant_aprobadas
        self.cant_desechadas = cant_desechadas
        self.cant_inconclusas = cant_inconclusas
        self.porcentaje_aprobadas = porcentaje_aprobadas
        self.porcentaje_desechadas = porcentaje_desechadas
        self.porcentaje_inconclusas = porcentaje_inconclusas

        autores = db.get_autores(PERIODO)
        autores_dict = {}
        for autor in autores:
            if autor['clave'] not in autores_dict:
                autores_dict[autor['clave']] = []
            autores_dict[autor['clave']].append(autor)
        self.autores_dict = autores_dict

        adhesiones = db.get_adhesiones(PERIODO)
        adhesiones_dict = {}
        for adhesion in adhesiones:
            if adhesion['clave'] not in adhesiones_dict:
                adhesiones_dict[adhesion['clave']] = []
            adhesiones_dict[adhesion['clave']].append(adhesion)
        self.adhesiones_dict = adhesiones_dict

        comisiones = db.get_comisiones_asignadas(PERIODO)
        comisiones_dict = {}
        for comision in comisiones:
            if comision['clave'] not in comisiones_dict:
                comisiones_dict[comision['clave']] = []
            comisiones_dict[comision['clave']].append(comision['nombre'])
        self.comisiones_dict = comisiones_dict

        comisiones_iniciativas_dict = {}
        for comision in comisiones:
            comisiones_iniciativas_dict[comision['nombre']] = []
        for numero in self.ini_dict:
            for nombre in self.comisiones_dict[numero]:
                comisiones_iniciativas_dict[nombre].append(numero)
        self.comisiones_iniciativas_dict = dict(sorted(
            comisiones_iniciativas_dict.items(), key=lambda e: e[0] or ''))

        procesos = db.get_procesos(PERIODO)
        self.proc_dict = list2dict(procesos)
        documentos_todos = db.get_documentos_todos(PERIODO)
        self.documentos_todos = documentos_todos

        documentos_dic = {}
        for documento in documentos:
            clave = documento['clave']
            if clave not in documentos_dic:
                documentos_dic[clave] = []
            documentos_dic[clave].append(documento)
        self.documentos_dic = documentos_dic

        pattern = 'su ley de ingresos|ley de ingresos.*municipio'
        for clave in self.ini_dict:
            cambios = self.ini_dict[clave]['cambios']
            if re.search(pattern, cambios, re.I):
                valor = 'Ley de Ingresos'
                m = re.search('\d{4}', cambios)
                if m:
                    periodo = m.group()
                    valor += f' {periodo}'
                if 'Aprueba' not in cambios:
                    valor = 'Modificación ' + valor
                self.ini_dict[clave]['municipal'] = valor
            else:
                self.ini_dict[clave]['municipal'] = ''
        self.ingresos, self.otras = totales(self)

def calcula_intervalos_acumulado(documentos):
    '''Regresa acumulado_dict y doc_dict

    A cada elemento de doc_dict se le agrega ``intervalo`` con
    la cantidad de días entre su fecha de sesión y la del siguiente
    elemento.
    Si ``condicion`` de un elemento es 'Aprobado', se le agrega
    ``total`` con el acumulado de los intervalos anteriores y no se le
    agrega ``intervalo``.  A los siguientes elementos ya no
    se les agrega ``intervalo``'''

    acumulado_dict = {}
    doc_dict = {}
    clave_anterior = False
    for documento in documentos:
        if documento['clave'] != clave_anterior:
            documento_anterior = False
            if clave_anterior:
                acumulado_dict[clave_anterior] = acumulado
            acumulado = 0
            aprobado = False
            clave_anterior = documento['clave']
        documento_dict = dict(documento)
        if documento_anterior and not aprobado:
            fecha_anterior = date.fromisoformat(documento_anterior['fecha_sesion'])
            fecha_actual = date.fromisoformat(documento['fecha_sesion'])
            intervalo = fecha_actual - fecha_anterior
            acumulado += intervalo.days
            documento_anterior['intervalo'] = intervalo.days
            # documento_anterior['acumulado'] = acumulado
            if documento['condicion'] == 'Aprobado':
                documento_dict['total'] = acumulado
                aprobado = True
        if documento['clave'] not in doc_dict:
            doc_dict[documento['clave']] = [documento_dict]
        else:
            doc_dict[documento['clave']].append(documento_dict)
        documento_anterior = documento_dict
    if clave_anterior:
        acumulado_dict[clave_anterior] = acumulado
    return acumulado_dict, doc_dict


def list2dict(iniciativas):
    'Regresa un mapeo de clave a iniciativa'
    ini_dict = {}
    for iniciativa in iniciativas:
        dicc = dict([(key, iniciativa[key]) for key in iniciativa.keys()])
        ini_dict[iniciativa['clave']] = dicc
    return ini_dict
