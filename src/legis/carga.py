import json

from .misc import condicional
from .legis import descripcion_corta


def procesa_nota(legislatura, clave, db, ini_dict, elemento,
                 dry_run, yes, verbose):
    """Actualiza la columna ``nota`` en la base de datos ``db``.

    Muestra si hay un valor nuevo o actualizado, pregunta el usuario
    si acepta actualizar el registro en la tabla ``proceso``.


    Con ``dry_run`` solamente muestra el valor sin agregarlo.  Con
    ``yes`` muestra el valor nuevo y lo agrega.
    """
    status = ''
    nota = ini_dict[clave]['nota']
    if elemento['nota'] and not nota:
        print(f'{clave:>10} nueva nota:', elemento['nota'])
        status = 'nueva'
    elif elemento['nota'] != nota:
        print(f"{clave:>10} actualizar nota: {nota} -> {elemento['nota']}")
        status = 'actualizada'
    else:
        return status
    if condicional(' Actualizar nota? [Y/n]', ['', 'y'],
                   dry_run=dry_run, yes=yes):
        print('  actualizando')
        nota = elemento['nota']
        rows = db.update_proceso(legislatura.periodo.upper(), clave,
                                 'nota', nota)
        print(rows)
    return status


def procesa_tags(legislatura, clave, db, ini_dict, elemento,
                 dry_run, yes, verbose):
    """Actualiza la columna ``tags`` en la base de datos ``db``.

    Muestra si hay un valor nuevo o actualizado, pregunta el usuario
    si acepta actualizar el registro en la tabla ``proceso``.


    Con ``dry_run`` solamente muestra el valor sin agregarlo.  Con
    ``yes`` muestra el valor nuevo y lo agrega.
    """
    tags = ini_dict[clave]['tags']
    if not tags and elemento['tags']:
        print(f'{clave:>10} nuevo tags:', elemento['tags'])
    elif elemento['tags'] != tags:
        print(f"{clave:>10} actualizar tags: {tags} -> {elemento['tags']}")
    else:
        return
    if condicional(' Actualizar tag? [Y/n]', ['', 'y'],
                   dry_run=dry_run, yes=yes):
        print('  actualizando')
        tags = elemento['tags']
        rows = db.update_proceso(legislatura.periodo.upper(), clave,
                                 'tags', tags)
        print(rows)


def procesa_resumen(legislatura, clave, db, ini_dict, elemento,
                 dry_run, yes, verbose):
    """Actualiza la columna ``resumen`` en la base de datos ``db``.

    Muestra si hay un valor nuevo o actualizado, pregunta el usuario
    si acepta actualizar el registro en la tabla ``proceso``.


    Con ``dry_run`` solamente muestra el valor sin agregarlo.  Con
    ``yes`` muestra el valor nuevo y lo agrega.
    """
    resumen = ini_dict[clave]['resumen']
    if not resumen and elemento['resumen']:
        print(f'{clave:>10} nuevo resumen:', elemento['resumen'])
    elif elemento['resumen'] != resumen:
        print(f"{clave:>10} actualizar resumen: {resumen} -> {elemento['resumen']}")
    else:
        return
    if condicional(' Actualizar resumen? [Y/n]', ['', 'y'],
                   dry_run=dry_run, yes=yes):
        print('  actualizando')
        resumen = elemento['resumen']
        rows = db.update_proceso(legislatura.periodo.upper(), clave,
                                 'resumen', resumen)
        print(rows)


def procesa_ediciones(legislatura, db, ruta_archivo_ediciones,
                      dry_run, yes, verbose):
    """Agrega ediciones a la base de datos ``db``.

    Lee ``ruta_archivo_ediciones`` e itera en cada elemento para
    procesar ``nota``, ``resumen`` y ``tags`` llamando a
    :func:`~legis.carga.procesa_nota`,
    :func:`~legis.carga.procesa_resumen` y
    :func:`~legis.carga.procesa_tags` respectivamente.

    """

    nuevas = []
    ini_dict = legislatura.ini_dict
    print(len(ini_dict), 'iniciativas')
    elementos = json.load(open(ruta_archivo_ediciones))
    for elemento in elementos:
        if 'tema' in elemento:
            elemento['nota'] = elemento['tema']
        if 'numero' in elemento:
            elemento['clave'] = elemento['numero']
        if 'tags' not in elemento:
            elemento['tags'] = ''
        clave = elemento['clave']
        if clave in ini_dict:
            status = procesa_nota(legislatura, clave, db, ini_dict, elemento,
                                  dry_run, yes, verbose)
            if status == 'nueva':
                nuevas.append((clave, elemento['nota']))
            procesa_tags(legislatura, clave, db, ini_dict, elemento,
                         dry_run, yes, verbose)
            procesa_resumen(legislatura, clave, db, ini_dict, elemento,
                         dry_run, yes, verbose)
        else:
            print(f'{clave:>10} no encontrada')
    if verbose:
        if nuevas:
            print(f'\n{len(nuevas)} iniciativas con tema y resumen nuevo:')
            for clave, tema in nuevas:
                print(clave, tema)


def procesa_cambios(legislatura, db, dry_run, yes, verbose):
    """Asigna valor a la columna ``cambios`` en la base de datos ``db``.

    Itera en cada elemento de legislatura.ini_dict, utilizando
    :func:`legis.legis.descripcion_corta` para generar el valor.
    Muestra si es un nuevo valor o si se actualiza el existente.
    Pregunta al usuario si acepta actualizar la tabla ``proceso`` en
    la base de datos ``db``

    Con ``dry_run`` solamente muestra el valor sin agregarlo.  Con
    ``yes`` muestra el valor nuevo y lo agrega.

    """

    ini_dict = legislatura.ini_dict
    for clave in ini_dict:
        cambios = ini_dict[clave]['cambios']
        descr_corta = descripcion_corta(ini_dict[clave]['descripcion'])
        if not cambios:
            print(f'{clave:>10} nuevo cambios:', descr_corta)
        elif descr_corta != cambios:
            print(f"{clave:>10} actualizar: {cambios} -> {descr_corta}")
        else:
            continue
        if condicional(' Actualizar cambios? [Y/n]', ['', 'y'],
                       dry_run=dry_run, yes=yes):
            print('  actualizando')
            rows = db.update_proceso(legislatura.periodo.upper(), clave,
                                     'cambios', descr_corta)
            print(rows)

