

def condicional(prompt, valid_responses, dry_run=False, yes=False):
    '''Muestra prompt, valida la respuesta y regresa True/False.

    dry_run regresa False, yes regresa True.  dry_run tiene prioridad'''
    if dry_run:
        return False
    elif yes:
        return True
    respuesta = input(prompt)
    return respuesta in valid_responses
