import unittest


class CalculaIntervalosAcumulado(unittest.TestCase):

    def test_normal(self):
        from legis.modelo import calcula_intervalos_acumulado

        d1 = {'clave': '1', 'fecha_sesion': '2022-01-01',
              'condicion': ''}
        d2 = {'clave': '1', 'fecha_sesion': '2022-01-10',
              'condicion': 'Aprobado'}
        documentos = [d1, d2]

        r1 = {'clave': '1', 'fecha_sesion': '2022-01-01',
              'condicion': '', 'intervalo': 9}
        r2 = {'clave': '1', 'fecha_sesion': '2022-01-10',
              'condicion': 'Aprobado', 'total': 9}
        doc_dict_r = {'1': [r1, r2]}

        acumulado_dict_r = {'1': 9}

        acumulado_dict, doc_dict = calcula_intervalos_acumulado(documentos)
        self.assertEqual(doc_dict, doc_dict_r)
        self.assertEqual(acumulado_dict, acumulado_dict_r)

    def test_aprobado_siguiente(self):
        from legis.modelo import calcula_intervalos_acumulado

        d1 = {'clave': '1', 'fecha_sesion': '2022-01-01',
              'condicion': ''}
        d2 = {'clave': '1', 'fecha_sesion': '2022-01-10',
              'condicion': 'Aprobado'}
        d3 = {'clave': '1', 'fecha_sesion': '2022-01-20',
              'condicion': ''}
        documentos = [d1, d2, d3]

        r1 = {'clave': '1', 'fecha_sesion': '2022-01-01',
              'condicion': '', 'intervalo': 9}
        r2 = {'clave': '1', 'fecha_sesion': '2022-01-10',
              'condicion': 'Aprobado', 'total': 9}
        r3 = {'clave': '1', 'fecha_sesion': '2022-01-20',
              'condicion': ''}
        doc_dict_r = {'1': [r1, r2, r3]}

        acumulado_dict_r = {'1': 9}

        acumulado_dict, doc_dict = calcula_intervalos_acumulado(documentos)
        self.assertEqual(doc_dict, doc_dict_r)
        self.assertEqual(acumulado_dict, acumulado_dict_r)


class EstadoIniciativa(unittest.TestCase):

    def test_pendiente(self):
        from legis.modelo import estado_iniciativa

        doc_dict = {'1': [{'tipo': 'Iniciativa de Ley',
                           'condicion': 'Para agendar en sesión',
                           'estado': 'Ingreso Administrativo'}]
                    }
        res = estado_iniciativa(doc_dict, '1')
        self.assertEqual(res, 'Pendiente')

    def test_aprobado(self):
        from legis.modelo import estado_iniciativa

        doc_dict = {'1': [{'tipo': 'Minuta de Decreto',
                           'condicion': 'Aprobado',
                           'estado': 'Concluido'}]
                    }
        res = estado_iniciativa(doc_dict, '1')
        self.assertEqual(res, 'Aprobado')

    def test_aprobado_estudio(self):
        from legis.modelo import estado_iniciativa

        doc_dict = {'1': [{'tipo': 'Minuta de Decreto',
                           'condicion': 'Aprobado',
                           'estado': 'Estudio'}]
                    }
        res = estado_iniciativa(doc_dict, '1')
        self.assertEqual(res, 'Aprobado')

    def test_desechado(self):
        from legis.modelo import estado_iniciativa

        doc_dict = {'1': [{'tipo': 'Acuerdo Legislativo',
                           'condicion': 'Aprobado',
                           'estado': 'Concluido'}]
                    }
        res = estado_iniciativa(doc_dict, '1')
        self.assertEqual(res, 'Desechado')

    def test_estudio(self):
        from legis.modelo import estado_iniciativa

        doc_dict = {'1': [{'tipo': 'Acuerdo Legislativo',
                           'condicion': 'Aprobado',
                           'estado': 'Estudio'}]
                    }
        res = estado_iniciativa(doc_dict, '1')
        self.assertEqual(res, 'Pendiente')
