import unittest


class Condicional(unittest.TestCase):

    def setUp(sefl):
        from unittest.mock import Mock
        import legis.misc as misc

        misc.input = Mock()

    def tearDown(self):
        import legis.misc as misc
        del misc.input

    def test_input_prompt(self):
        from legis.misc import condicional
        import legis.misc as misc

        resultado = condicional('prompt', [])
        misc.input.assert_called_with('prompt')

    def test_input_y(self):
        from legis.misc import condicional
        import legis.misc as misc
        misc.input.return_value = 'y'

        resultado = condicional('', ['y'])
        self.assertEqual(resultado, True)

    def test_input_n(self):
        from legis.misc import condicional
        import legis.misc as misc
        misc.input.return_value = 'n'

        resultado = condicional('', ['y'])
        self.assertEqual(resultado, False)

    def test_input_enter(self):
        from legis.misc import condicional
        import legis.misc as misc
        misc.input.return_value = ''

        resultado = condicional('', ['y'])
        self.assertEqual(resultado, False)

    def test_dry_run(self):
        from legis.misc import condicional

        resultado = condicional('', [], dry_run=True)
        self.assertEqual(resultado, False)

    def test_yes(self):
        from legis.misc import condicional

        resultado = condicional('', [], yes=True)
        self.assertEqual(resultado, True)
