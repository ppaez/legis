import unittest
import os
from sqlite3 import IntegrityError


with open(os.environ['LEGIS_SCHEMA']) as f:
    init_sql = f.read()

with open('tests/data.sql') as f:
    data_sql = f.read()


class GetAdhesiones(unittest.TestCase):

    def setUp(self):
        from legis.db import DB
        self.db = DB(':memory:')
        self.db.connection.executescript(init_sql)
        self.db.connection.executescript(data_sql)


    def test_normal(self):
        db = self.db

        esperado = 'persona1'
        resultado = db.get_adhesiones('legislatura1', '1')
        self.assertEqual(len(resultado), 1)
        self.assertEqual(resultado[0]['nombre'], esperado)


class AsignaAdhesion(unittest.TestCase):

    def setUp(self):
        from legis.db import DB
        self.db = DB(':memory:')
        self.db.connection.executescript(init_sql)
        self.db.connection.executescript(data_sql)


    def test_duplicado(self):
        db = self.db

        self.assertRaises(IntegrityError, db.asigna_adhesion,
                          'legislatura1', '1', 'persona1')


class Autor(unittest.TestCase):

    def setUp(self):
        from legis.db import DB
        self.db = DB(':memory:')
        self.db.connection.executescript(init_sql)
        self.db.connection.executescript(data_sql)


    def test_asigna_autor(self):
        db = self.db

        resultado = db.inserta_autor('legislatura1', '1', 'persona1')
        self.assertEqual(resultado, 1)


    def test_remueve_autor(self):
        db = self.db

        resultado = db.remueve_autor('legislatura1', '1', 'persona1')
        self.assertEqual(resultado, 1)
