import unittest
from unittest.mock import Mock, MagicMock
from urllib.error import URLError


class DescargaDocumento(unittest.TestCase):

    def setUp(self):
        import legis.descarga

        self.urlopen = legis.descarga.urlopen
        response = Mock()
        response.read = Mock(return_value=b'data')
        response.status = 0
        response.reason = 'error'
        response.headers = 'headers'
        legis.descarga.urlopen = Mock(return_value=response)
        self.Request = legis.descarga.Request
        legis.descarga.Request = Mock()
        legis.descarga.open = MagicMock()
        self.datetime = legis.descarga.datetime
        legis.descarga.datetime = Mock()
        legis.descarga.datetime.now.return_value = 0

    def tearDown(self):
        import legis.descarga

        legis.descarga.urlopen = self.urlopen
        legis.descarga.Request = self.Request
        del legis.descarga.open

    def test_normal(self):
        from legis.descarga import descarga_documento

        esperado = ('ok', 0, 0, 'error', 4, 'headers')
        resultado = descarga_documento('file://url', 'archivo.pdf')
        self.assertEqual(resultado, esperado)

    def test_error(self):
        from legis.descarga import descarga_documento
        import legis.descarga

        legis.descarga.urlopen.side_effect = URLError('error')

        esperado = 'error', 0, 'error', 0, 0
        resultado = descarga_documento('file://url', 'archivo.pdf')
        self.assertEqual(resultado, esperado)
