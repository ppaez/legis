import unittest


class Descripcion_Corta(unittest.TestCase):

    def test_normal(self):
        from legis.legis import descripcion_corta

        text = 'prueba'
        esperado = 'prueba'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_codigo_f(self):
        from legis.legis import descripcion_corta

        text = '... (F.123)'
        esperado = '...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_codigo_f_espacio(self):
        from legis.legis import descripcion_corta

        text = '... (F. 123)'
        esperado = '...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_para_reformar(self):
        from legis.legis import descripcion_corta

        text = '... para reformar; ...'
        esperado = 'Reformar; ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_para_reformar_coma(self):
        from legis.legis import descripcion_corta

        text = '... para reformar, ...'
        esperado = 'Reformar, ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_para_adicionar(self):
        from legis.legis import descripcion_corta

        text = '... para adicionar ...'
        esperado = 'Adicionar ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_para_derogar(self):
        from legis.legis import descripcion_corta

        text = '... para derogar ...'
        esperado = 'Derogar ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_reforma(self):
        from legis.legis import descripcion_corta

        text = '... que reforma ...'
        esperado = 'Reforma ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_reforma_comma(self):
        from legis.legis import descripcion_corta

        text = '... que reforma, ...'
        esperado = 'Reforma, ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que__reforma(self):
        from legis.legis import descripcion_corta

        text = '... que  reforma ...'
        esperado = 'Reforma ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que___reforma(self):
        from legis.legis import descripcion_corta

        text = '... que   reforma ...'
        esperado = 'Reforma ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que__reforma_que_crea(self):
        from legis.legis import descripcion_corta

        text = '... que  reforma ... que crea ...'
        esperado = 'Reforma ... que crea ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_reforman(self):
        from legis.legis import descripcion_corta

        text = '... que reforman ...'
        esperado = 'Reforman ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_propone_reformar(self):
        from legis.legis import descripcion_corta

        text = '... que propone reformar ...'
        esperado = 'Reforma ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_propone_adicionar(self):
        from legis.legis import descripcion_corta

        text = '... que propone adicionar ...'
        esperado = 'Adiciona ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_deroga(self):
        from legis.legis import descripcion_corta

        text = '... que deroga ...'
        esperado = 'Deroga ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_abroga(self):
        from legis.legis import descripcion_corta

        text = '... que abroga ...'
        esperado = 'Abroga ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_Abroga(self):
        from legis.legis import descripcion_corta

        text = '... que Abroga ...'
        esperado = 'Abroga ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_modifica(self):
        from legis.legis import descripcion_corta

        text = '... que modifica ...'
        esperado = 'Modifica ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_mediante_el_cual_se_modifica(self):
        from legis.legis import descripcion_corta

        text = '... mediante el cual modifica ...'
        esperado = 'Modifica ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_adiciona(self):
        from legis.legis import descripcion_corta

        text = '... que adiciona ...'
        esperado = 'Adiciona ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_expide(self):
        from legis.legis import descripcion_corta

        text = '... que expide ...'
        esperado = 'Expide ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_crea(self):
        from legis.legis import descripcion_corta

        text = '... que crea ...'
        esperado = 'Crea ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_aprueba(self):
        from legis.legis import descripcion_corta

        text = '... que aprueba ...'
        esperado = 'Aprueba ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_que_solicita_corregir(self):
        from legis.legis import descripcion_corta

        text = '... que solicita corregir ...'
        esperado = 'Corrige ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_se_modifican(self):
        from legis.legis import descripcion_corta

        text = '... se modifican ...'
        esperado = 'Modifica ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_se_reforma(self):
        from legis.legis import descripcion_corta

        text = '... se reforma ...'
        esperado = 'Reforma ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_modificacion_a(self):
        from legis.legis import descripcion_corta

        text = '... modificación a ...'
        esperado = 'Modifica ...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_presentada(self):
        from legis.legis import descripcion_corta

        text = '... presentada mediante oficio ...'
        esperado = '...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_coma_presentada(self):
        from legis.legis import descripcion_corta

        text = '...  presentada mediante oficio ...'
        esperado = '...'
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_c_anexo(self):
        from legis.legis import descripcion_corta

        text = '(cd/anexo)'
        esperado = ''
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

    def test_usb_anexo(self):
        from legis.legis import descripcion_corta

        text = '(usb/anexo)'
        esperado = ''
        resultado = descripcion_corta(text)
        self.assertEqual(resultado, esperado)

